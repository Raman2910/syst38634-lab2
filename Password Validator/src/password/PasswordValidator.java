package password;

public class PasswordValidator {
	
	public static int MIN_LENGTH = 8;

	public static boolean isValidLength(String password) {
		if(password != null) {
		return password.trim().length() >= MIN_LENGTH;
	}
			return false;
	}
	
	public static boolean hasEnoughDigits(String password){
		return false;
	}
	
	public static boolean isUpperCase(String password) {
		if(password != null) {
			for(int x = 0; x < password.length(); x++) {
				char c = password.charAt(x);
				if(Character.isUpperCase(c)) {
					return true;
				}
			}
		}
		return false;
	}
}
