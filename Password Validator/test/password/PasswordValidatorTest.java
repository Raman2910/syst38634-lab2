package password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345667890"));
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(" 		"));
	}
	
	// Unit tests for IsUpperCase() method

	@Test
	public void testIsUpperCaseRegular() {
		assertTrue("Password doesn't contain any uppercase letter", PasswordValidator.isUpperCase("Password"));
	}
	
	@Test
	public void testIsUpperCaseException() {
		assertFalse("Password doesn't contain any uppercase letter", PasswordValidator.isUpperCase(null));
	}
	
	@Test
	public void testIsUpperCaseExceptionSpaces() {
		assertFalse("Password doesn't contain any uppercase letter", PasswordValidator.isUpperCase(" 		"));
	}
}
